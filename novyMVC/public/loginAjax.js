$(document).on('click', '#login_btn' ,function(e) {

    e.preventDefault();

    let login 	 = $('#login').val();
    let password = $('#password').val();

    $.ajax({
        url: '?c=Auth&a=login',
        type: 'post',
        data:
            {   login : login,
                password : password,
            },
        success: function(response, textStatus, XmlHttpRequest){
            if (response.successfullLogin != undefined) {
                document.location.href= '?c=home';
            }
            if (response.emailWrong != undefined)
            {
                alert(response.emailWrong);
                //document.getElementById("login").value = response.emailWrong;
            }
            if(response.passwordWrong != undefined)
            {
                alert(response.passwordWrong);
                //document.getElementById("password").value = response.passwordWrong;
            }


        },
        error: function (jqXHR, exception){
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            console.log(msg);
        }
        /*function(response){
            const json = jQuery.parseJSON(response);
            $('#usernameError').innerText = json.usernameError;
            $('#emailError').innerText = json.emailError;
        }*/

    });


});

/*function togglePassword() {
    let pass = document.getElementById("password");
    if (pass.type === "password") {
        pass.type = "text";
    } else {
        pass.type = "password";
    }
}

$(document).on('click', '#password' ,function(e) {
    let pass = document.getElementById("password");
    pass.value = "";
    if (pass.type === "text")
    {
        pass.type = "password";
    }
})*/

//$(document).on()
//