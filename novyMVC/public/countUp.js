let scrollCounter = 0;
$(window).scroll(function() {
    if (scrollCounter == 0)
    {
        let hT = $('#countUpStart').offset().top,
            hH = $('#countUpStart').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-wH)){
            scrollCounter++;
            const counters = document.querySelectorAll('.counter');
            for(let n of counters) {
                const updateCount = () => {

                    const target = + n.getAttribute('data-target');
                    const count = + n.innerText;
                    const speed = 1000;
                    const inc = target / speed;
                    if(count < target) {
                        n.innerText = Math.ceil(count + inc);
                        setTimeout(updateCount, 30);
                    } else {
                        n.innerText = target;
                        n.innerText += " minút";
                    }
                }
                updateCount();
            }
        }
    }

});
