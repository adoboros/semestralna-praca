$(document).on('click', '#changePassword_btn' ,function(e) {

    e.preventDefault();

    let oldPassword = $('#oldPassword').val();
    let password = $('#password').val();
    let secondPassword = $('#secondPassword').val();

    if (oldPassword == '') { //prazdne heslo
        alert('Prosím zadajte aktuálne heslo!');
    } else if (password == '') {
        alert('Prosím zadajte nové heslo!');
    } else if (password.length < 6) { //minimalna dlzka hesla 6 znakov
        alert('Heslo musí obsahovať minimálne 6 znakov!');
    } else if (password != secondPassword) { //nezhodujuce sa hesla
        alert('Heslá sa musia zhodovať!');
    } else {

        jQuery.ajax({
            url: '?c=Auth&a=changePassword',
            type: 'post',
            dataType: 'text',
            data:
                {
                    oldPassword: oldPassword,
                    password: password,
                    secondPassword: secondPassword,
                },
            success: function (responseJ, textStatus, XmlHttpRequest) {
                response = JSON.parse(responseJ);
                if (response.changePasswordSuccess != undefined) {
                    alert(response.changePasswordSuccess);
                    document.location.href = '?c=info&a=profile';
/*                    let alertSuc = document.getElementById("changePasswordSuc");
                    document.getElementById("changePasswordSuc").innerText = response.changePasswordSuccess;
                    document.getElementById("changePasswordSuc").style.display = "block";
                    function removeAlert() {
                        document.getElementById("changePasswordSuc").style.display = "none";
                    }
                    setTimeout(removeAlert(), 5000);*/

                    //
                }
                if (response.oldPasswordWrong != undefined) {
                    alert(response.oldPasswordWrong);
                    //document.getElementById("login").value = response.emailWrong;
                }
                if (response.passwordError != undefined) {
                    alert(response.passwordError);
                    //document.getElementById("password").value = response.passwordWrong;
                }


            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            }
            /*function(response){
                const json = jQuery.parseJSON(response);
                $('#usernameError').innerText = json.usernameError;
                $('#emailError').innerText = json.emailError;
            }*/

        });
    }

});

/*function togglePassword() {
    let pass = document.getElementById("password");
    if (pass.type === "password") {
        pass.type = "text";
    } else {
        pass.type = "password";
    }
}

$(document).on('click', '#password' ,function(e) {
    togglePassword();
})*/

//$(document).on()
//