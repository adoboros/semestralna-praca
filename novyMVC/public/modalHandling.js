$(document).ready(function () {
    $('.editbtn').on('click', function() {
        $('#editmodal').modal('show');

        $tr = $(this).closest('tr');

        let data = $tr.children("td").map(function() {
            return $(this).text();
        }).get();
        data[1] = data[1].replace(/(\r\n|\n|\r)/gm, "");
        data[1] = data[1].replace(/\s/g, "");
        console.log(data);
        $('#updateRoom_id').val(data[0]);
        $('#nameUpdate').val(data[1]);
        $('#contactUpdate').val(data[2]);
        $('#pscUpdate').val(data[3]);
        $('#streetUpdate').val(data[4]);
        $('#townUpdate').val(data[5]);
        $('#minutesUpdate').val(data[6]);
        $('#descUpdate').val(data[7]);
    });
});

function showModal()
{
    if(document.readyState === 'ready' || document.readyState === 'complete') {
        document.getElementById("addEscapeButtonModal").click();
        console.log("Ready")
    } else {
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                document.getElementById("addEscapeButtonModal").click();
                console.log("Just now");
            }
        }
    }
}