
$(document).on('click', '#addEscape' ,function(e) {

        e.preventDefault();

        let name = $('#nameAdd').val();
        let town = $('#townAdd').val();
        let psc = $('#pscAdd').val();
        let rating = $('#ratingAdd').val();
        let street = $('#streetAdd').val();
        let contact = $('#contactAdd').val();
        let minutes = $('#minutesAdd').val();
        let files = $('#file')[0].files;


        if(name == ''){ // prazdne meno
            alert('Prosím zadajte názov!');
            //maybe $('#nameAdd').value = prosim
        }
        else if (name.length > 20) {
            alert('Názov nesmie obsahovať viac ako 20 znakov');
        }
        else if(isNaN(rating)){
            alert('Prosím hodnotenie zadajte ako číselnú hodnotu!');
        }
        else if(rating > 5 || rating < 0){ //prazdne heslo
            alert('Hodnotenie musí byť z intervalu 0-5!');
        }
        else if(contact.length != 13){ //minimalna dlzka hesla 6 znakov
            alert('Číslo musí byť v tvare +421.. a obsahovať 13 znakov.!');
        }
        else if(!/^[+][0-9]{1,3}[0-9]{9}$/.test(contact)){
            alert('Nesprávny formát telefónneho čísla !!');
        }

        else if (psc == '') {
            alert('Prosím zadajte psč!')
        }
        else if(psc.length != 5){
            alert('Psč musí obsahovať 5 znakov!');
        }
        else if (street == '') {
            alert('Prosím zadajte ulicu!')
        }
        /*else if (street.match(/^[A-Za-z]/i)) {
            alert('Ulica musí začínať písmenom!');
        }*/
        else if(town == ''){
            alert('Prosím zadajte mesto!');
        }
        /*else if (/^[a-zA-Z]/.test(town)) {
            alert('Mesto musí začínať písmenom!');
        }*/
        else if(minutes > 180 || minutes < 10){
            alert('Minutáž musí byť v rozmedzí <10; 180>!');
        }
        else if (files.length < 1) {
            alert('Prosím zvoľte súbor!');
        }
        else{
            let formData = new FormData($('#form_id')[0]);

            jQuery.ajax({
                url: '?c=Home&a=insertRoom',
                type: 'POST',
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,

                success: function(responseJ, textStatus, XmlHttpRequest){
                    response = JSON.parse(responseJ);
                    /*if (response.usernameError != undefined)
                    {
                        document.getElementById("username").value = response.usernameError;
                    }*/
                    if (response.nameError != undefined)
                    {
                        document.getElementById("nameAdd").value = response.nameError;
                    }
                    if (response.ratingError != undefined)
                    {
                        document.getElementById("ratingAdd").value = response.ratingError;
                    }
                    if (response.contactError != undefined)
                    {
                        document.getElementById("contactAdd").value = response.contactError;
                    }
                    if (response.pscError != undefined)
                    {
                        document.getElementById("pscAdd").value = response.pscError;
                    }
                    if (response.streetError != undefined)
                    {
                        document.getElementById("streetAdd").value = response.streetError;
                    }
                    if (response.townError != undefined)
                    {
                        document.getElementById("townAdd").value = response.townError;
                    }
                    if (response.minutesError != undefined)
                    {
                        document.getElementById("minutesAdd").value = response.minutesError;
                    }
                    if (response.descError != undefined)
                    {
                        document.getElementById("descAdd").value = response.descError;
                    }
                    if (response.fileError != undefined)
                    {
                        alert(response.fileError)
                    }
                    if (response.insertSuccess != undefined)
                    {
                        alert(response.insertSuccess);
                    }

                },

                error: function (jqXHR, exception){
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                }


            });

        }
    });


//$(document).on()
//