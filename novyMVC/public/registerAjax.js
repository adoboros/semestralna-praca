$(document).on('click', '#register_btn' ,function(e) {

    e.preventDefault();

    let username = $('#username').val();
    let email 	 = $('#email').val();
    let password = $('#password').val();
    let second_password = $('#secondPassword').val();

    let atpos  = email.indexOf('@');
    let dotpos = email.lastIndexOf('.');

    if(username == ''){ // prazdne meno
        alert('Prosím zadajte meno!');
    }
    else if(!/^[a-z A-Z 0-9]+$/.test(username)){ // check username allowed capital and small letters
        alert('username only capital and small letters are allowed !!');
    }
    else if(email == ''){ //check email not empty
        alert('Prosím zadajte email!');
    }
    else if(atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length){ //spravny email
        alert('Prosím zadajte správnu emailovú adresu!');
    }
    else if(password == ''){ //prazdne heslo
        alert('Prosím zadajte heslo!');
    }
    else if(password.length < 6){ //minimalna dlzka hesla 6 znakov
        alert('Heslo musí obsahovať minimálne 6 znakov!');
    }
    else if(password != second_password){ //nezhodujuce sa hesla
        alert('Heslá sa musia zhodovať!');
    }
    else{
        $.ajax({
        url: '?c=Auth&a=register',
        type: 'post',
        data:
            {username : username,
                email : email,
                password : password
            },
            success: function(response, textStatus, XmlHttpRequest){
                /*if (response.usernameError != undefined)
                {
                    document.getElementById("username").value = response.usernameError;
                }*/
                /*let inputUsername = document.getElementById("username").value;
                let inputEmail = document.getElementById("email").value;
                document.getElementById("username").value = inputUsername + " - " + response.usernameError;
                document.getElementById("email").value = inputEmail + " - " + response.emailError;*/
                if (response.usernameError != undefined)
                {
                    alert(response.usernameError);
                }

                if (response.emailError != undefined)
                {
                    alert(response.emailError);
                }

                if (response.successfull != undefined)
                {
                    $suc = response.successfull;
                    $('#registration_form')[0].reset();
                    //swal("Registrácia", $suc);
                    alert($suc);
                }

            },
            error: function (jqXHR, exception){
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            }
            /*function(response){
                const json = jQuery.parseJSON(response);
                $('#usernameError').innerText = json.usernameError;
                $('#emailError').innerText = json.emailError;
            }*/

        });

    }
});

//$(document).on()
//