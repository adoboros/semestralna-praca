

$(document).on('click', '#updateEscape' ,function(e) {
    e.preventDefault();

    let name = $('#nameUpdate').val();
    let town = $('#townUpdate').val();
    let psc = $('#pscUpdate').val();
    let street = $('#streetUpdate').val();
    let contact = $('#contactUpdate').val();
    let minutes = $('#minutesUpdate').val();


    if(name == ''){ // prazdne meno
        alert('Prosím zadajte názov!');
        //maybe $('#nameAdd').value = prosim
    }
    else if (name.length > 20) {
        alert('Názov nesmie obsahovať viac ako 20 znakov');
    }
    else if(contact.length != 13){ //minimalna dlzka hesla 6 znakov
        alert('Číslo musí byť v tvare +421.. a obsahovať 13 znakov.!');
    }
    else if(!/^[+][0-9]{1,3}[0-9]{9}$/.test(contact)){
        alert('Nesprávny formát telefónneho čísla !!');
    }

    else if (psc == '') {
        alert('Prosím zadajte psč!')
    }
    else if(psc.length != 5){
        alert('Psč musí obsahovať 5 znakov!');
    }
    else if (street == '') {
        alert('Prosím zadajte ulicu!')
    }
    /*else if (street.match(/^[A-Za-z]/i)) {
        alert('Ulica musí začínať písmenom!');
    }*/
    else if(town == ''){
        alert('Prosím zadajte mesto!');
    }
    /*else if (/^[a-zA-Z]/.test(town)) {
        alert('Mesto musí začínať písmenom!');
    }*/
    else if(minutes > 180 || minutes < 10){
        alert('Minutáž musí byť v rozmedzí <10; 180>!');
    }

    else{
        let formData = new FormData($('#updateEscapeForm')[0]);

        jQuery.ajax({
            url: '?c=home&a=updateRoom',
            type: 'POST',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: formData,

            success: function(responseJ, textStatus, XmlHttpRequest){
                response = JSON.parse(responseJ);
                /*if (response.usernameError != undefined)
                {
                    document.getElementById("username").value = response.usernameError;
                }*/
                if (response.nameError != undefined)
                {
                    document.getElementById("nameUpdate").value = response.nameError;
                }
                if (response.contactError != undefined)
                {
                    document.getElementById("contactUpdate").value = response.contactError;
                }
                if (response.pscError != undefined)
                {
                    document.getElementById("pscUpdate").value = response.pscError;
                }
                if (response.streetError != undefined)
                {
                    document.getElementById("streetUpdate").value = response.streetError;
                }
                if (response.townError != undefined)
                {
                    document.getElementById("townUpdate").value = response.townError;
                }
                if (response.minutesError != undefined)
                {
                    document.getElementById("minutesUpdate").value = response.minutesError;
                }
                if (response.descError != undefined)
                {
                    document.getElementById("descUpdate").value = response.descError;
                }
                if (response.fileError != undefined)
                {
                    alert(response.fileError)
                }
                if (response.updateSuccess != undefined)
                {
                    alert(response.updateSuccess);
                }

            },

            error: function (jqXHR, exception){
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                console.log(msg);
            }


        });

    }
});


//$(document).on()
//