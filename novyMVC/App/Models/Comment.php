<?php

namespace App\Models;

class Comment extends \App\Core\Model
{

    public int $id = 0;
    public ?string $user_email = null;
    public int $room_id = 0;
    public ?string $text = null;
    public ?string $date = null;

    public function __construct()
    {
    }

    static public function setDbColumns()
    {
        return ['id', 'user_email', 'room_id', 'text', 'date'];
    }

    static public function setTableName()
    {
        return "comment";
    }

    public function getOwner()
    {
        return User::getAll('email = ?', [$this->user_email]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->user_email;
    }

    /**
     * @param string $user_email
     */
    public function setUserEmail(string $user_email): void
    {
        $this->user_email = $user_email;
    }

    /**
     * @return int
     */
    public function getRoomId(): int
    {
        return $this->room_id;
    }

    /**
     * @param int $room_id
     */
    public function setRoomId(int $room_id): void
    {
        $this->room_id = $room_id;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string|null
     */
    public function getDate(): ?string
    {
        return $this->date;
    }

    /**
     * @param string|null $date
     */
    public function setDate(?string $date): void
    {
        $this->date = $date;
    }
}