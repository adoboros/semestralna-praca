<?php
namespace App\Models;

use App\escapeHandler;

class User extends \App\Core\Model
{

    public int $id = 0;
    public ?string $username = null;
    public ?string $email = null;
    public ?string $password = null;

    public function __construct(
    )
    {
    }

    static public function setDbColumns()
    {
        return ['id', 'email', 'username', 'password'];
    }

    static public function setTableName()
    {
        return "users";
    }

    public function getRooms()
    {
        return escapeHandler::getUsersRooms($this->email);
    }

    public function getComments()
    {
        return Comment::getAll('user_email = ?', [$this->email] );
    }

    public function getFavorites()
    {
        return escapeHandler::getUserFavorites();
    }

    public function isInFavorites($room_id)
    {
        $rooms = escapeHandler::getUserFavorites();
        foreach ($rooms as $room)
        {
            if ($room->getId() == $room_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

}