<?php
namespace App\Models;

class EscapeRoom extends \App\Core\Model
{
    public int $id = 0;
    public ?string $user_email = null;
    public ?string $nazov = null;
    public float $hodnotenie = 0;
    public int $pocet_hodnoteni = 0;
    public ?string $kontakt = null;
    public ?string $psc = null;
    public ?string $ulica = null;
    public ?string $mesto = null;
    public int $minutaz = 0;
    public ?string $image = null;
    public ?string $opis = null;

    public function __construct(

    )
    {
    }

    static public function setDbColumns()
    {
        return ['id', 'user_email', 'nazov', 'hodnotenie', 'pocet_hodnoteni', 'kontakt', 'psc', 'ulica', 'mesto', 'minutaz', 'image', 'opis'];
    }

    static public function setTableName()
    {
        return "rooms";
    }

    public function getComments()
    {
        return Comment::getAll('room_id = ?', [$this->id] );
    }

    public function saveImage()
    {
        /*if ($_FILES['file']['error'] == UPLOAD_ERR_OK)
        {
            //$this->imageName = dat;
            move_uploaded_file($_FILES)
        }*/
    }

    public function getOwner()
    {
        return User::getAll('user_email = ?', [$this->user_email]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserEmail()
    {
        return $this->user_email;
    }

    /**
     * @return string|null
     */
    public function getNazov(): ?string
    {
        return $this->nazov;
    }

    /**
     * @param string|null $nazov
     */
    public function setNazov(?string $nazov): void
    {
        $this->nazov = $nazov;
    }

    /**
     * @return float|int
     */
    public function getHodnotenie(): float
    {
        return $this->hodnotenie;
    }

    /**
     * @param float|int $hodnotenie
     */
    public function setHodnotenie(float $hodnotenie): void
    {
        $this->hodnotenie = $hodnotenie;
    }

    /**
     * @return string|null
     */
    public function getKontakt(): ?string
    {
        return $this->kontakt;
    }

    /**
     * @param string|null $kontakt
     */
    public function setKontakt(?string $kontakt): void
    {
        $this->kontakt = $kontakt;
    }

    /**
     * @return string|null
     */
    public function getPsc(): ?string
    {
        return $this->psc;
    }

    /**
     * @param string|null $psc
     */
    public function setPsc(?string $psc): void
    {
        $this->psc = $psc;
    }

    /**
     * @return string|null
     */
    public function getUlica(): ?string
    {
        return $this->ulica;
    }

    /**
     * @param string|null $ulica
     */
    public function setUlica(?string $ulica): void
    {
        $this->ulica = $ulica;
    }

    /**
     * @return string|null
     */
    public function getMesto(): ?string
    {
        return $this->mesto;
    }

    /**
     * @param string|null $mesto
     */
    public function setMesto(?string $mesto): void
    {
        $this->mesto = $mesto;
    }

    /**
     * @return int
     */
    public function getMinutaz(): int
    {
        return $this->minutaz;
    }

    /**
     * @param int $minutaz
     */
    public function setMinutaz(int $minutaz): void
    {
        $this->minutaz = $minutaz;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage(?string $image)
    {
        $this->image = $image;
    }

    /**
     * @param string $user_email
     */
    public function setUserEmail(string $user_email): void
    {
        $this->user_email = $user_email;
    }

    /**
     * @return string|null
     */
    public function getOpis(): ?string
    {
        return $this->opis;
    }

    /**
     * @param string|null $opis
     */
    public function setOpis(?string $opis): void
    {
        $this->opis = $opis;
    }

    /**
     * @return int
     */
    public function getPocetHodnoteni(): int
    {
        return $this->pocet_hodnoteni;
    }

    /**
     * @param int $pocet_hodnoteni
     */
    public function setPocetHodnoteni(int $pocet_hodnoteni): void
    {
        $this->pocet_hodnoteni = $pocet_hodnoteni;
    }
}