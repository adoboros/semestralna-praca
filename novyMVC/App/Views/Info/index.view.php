<?php /** @var Array $data */ ?>
<?php if (\App\Auth::isLogged()) { ?>
<div class="infoBiela">
    <div class="container">
        <div class="d-flex justify-content-between">
            <?php $room = $data['room'];
            $nazov = $room->getNazov();
            $mesto = $room->getMesto();
            $minutaz = $room->getMinutaz(); ?>
            <div class="card align-self-start">
                <img src="<?= \App\Config\Configuration::UPLOAD_DIR . $room->getImage() ?>" class="img-fluid" alt="Náhľad nie je k dispozícii.">
                <div class="card-body">
                    <h5 class="card-title" id="previewName"><?php echo $nazov ?></h5>
                    <p class="card-text" id="previewTown"><?php echo $mesto ?></p>
                    <p class="card-text" id="previewTime"><?php echo $minutaz ?> minút</p>
                    <?php if (!\App\Auth::getLoggedUser()->isInFavorites($room->getId())) { ?>
                    <a href="?c=info&a=addFavorite&id=<?php echo $room->getId() ?>" class="btn btn-warning stretched-link">Pridať k obľúbeným</a> <!-- len ak tam nie je -->
                    <?php } ?>
                </div>

            </div>

            <div class="mainInfo">
                <div>
                NÁZOV <div class="yellowText" ><?php echo $nazov ?></div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        MESTO <div class="yellowText"><?php echo $mesto ?></div>
                    </div>
                    <div class="col-md-6">
                        PSČ <div class="yellowText"><?php echo $room->getPsc() ?></div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        ULICA <div class="yellowText"><?php echo $room->getUlica() ?></div>
                    </div>
                    <div class="col-md-6">
                        KONTAKT <div class="yellowText"><?php echo $room->getKontakt() ?></div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-6">
                        TRVANIE <div class="yellowText"><?php echo $minutaz ?> minút</div>
                    </div>
                    <div class="col-md-6">
                        HODNOTENIE <div class="yellowText"><?php echo $room->getHodnotenie() ?> / 5</div>
                    </div>
                </div>


            </div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<div class="infoSeda">
    <h1>Opis</h1>
    <div class="description">
        <?php $desc =  $room->getOpis();
        if (!$desc == "") {
            echo $desc;
        } else { ?>
        Pre túto escape room nebol zadaný žiadny opis
        <?php } ?>
    </div>

</div>

<div class="infoBiela">
    <div class="container">
        <h1>Komentáre</h1>
        <div id="comment">
    <?php foreach ($room->getComments() as $comment) { ?>
        <div class="d-flex justify-content-center commentFlex">
            <div class="comment mt-4 text-justify" >
                <h4><?php $owner = $comment->getOwner()[0];
                    echo $owner->getUsername(); ?></h4> <span>- <?php echo $comment->getDate() ?></span> <br>
                <p><?php echo $comment->getText() ?></p>
            </div>
        </div>
    <?php } ?>
        </div>
        <div class="d-flex justify-content-center">
            <form id="newCommentTextArea" method="post" action="?c=info&a=addComment&id=<?php echo $room->getId() ?>">
                <textarea class="form-control" name="commentText" placeholder="Váš komentár" required ></textarea>
                <button type="submit" class="btn btn-warning" value="Odoslať">Odoslať</button>
            </form>
        </div>
        <div class="d-flex justify-content-center">
            <form id="addRatingForm" method="post" action="?c=info&a=addRating&id=<?php echo $room->getId() ?>">
                <input class="form-control" name="rating" type="number" min="0" max="5" placeholder="Vaše hodnotenie" required >

                <button type="submit" class="btn btn-warning" value="Odoslať">Odoslať</button>
            </form>
        </div>
    </div>
</div>
<?php } ?>