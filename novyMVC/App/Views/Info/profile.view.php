<?php /** @var Array $data */ ?>
<?php if (\App\Auth::isLogged()) { ?>
    <script src="public/modalHandling.js"></script>
    <script src="public/updateEscapeAjax.js"></script>
    <div class="infoBiela">
        <div class="container">
            <div class="d-flex justify-content-center">
                <?php $user = $data['user'][0]; ?>
                <?php if(isset($data['error']))
                {
                    $error = $data['error'];
                  echo  '<script type="text/javascript">alert("Chyba: ' . $error . '")</script>';
                }?>
                <div class="mainInfo">
                    <div class="row">
                        <div class="col-md-6">
                            MENO <div class="yellowText"><?php echo $user->getUsername() ?></div>
                        </div>
                        <div class="col-md-6">
                            EMAIL <div class="yellowText"><?php echo $user->getEmail() ?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="?c=auth&a=changePasswordForm" class="btn btn-warning">Zmeniť heslo</a>
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="btn btn-warning" id="deleteUser">Zmazať účet</a>
                        </div>
                    </div>
                    <div class="row" id="deleteConfirm" style="display: none">
                        <form method="post" action="?c=auth&a=deleteUser">
                            <div class="col-md-4">
                                <input type="password" name="deletePassConfirm" placeholder="Heslo">
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-warning" value="Odoslať">Potvrdiť</button>
                            </div>
                        </form>
                    </div>

                    <!--<div class="d-flex justify-content-start" id="deleteConfirm">


                    </div>-->



                </div>
            </div>
        </div>
    </div>

    <div class="infoSeda" id="userRoomsTable">
        <div id="userEscapes">
            Moje Escapes
        </div>
        <div class="table-responsive-lg">
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Názov</th>
                    <th>Kontakt</th>
                    <th>Psč</th>
                    <th>Ulica</th>
                    <th>Mesto</th>
                    <th>Minutáž</th>
                    <th>Akcie</th>
                    <th style="display: none">Opis</th>
                </tr>
                <?php
                $userRooms = $user->getRooms();
                foreach ($userRooms as $room) {  ?>
                    <tr>
                        <td style="width: 30px"><?php echo $room->getId(); ?></td>
                        <td>
                            <a class="userEscapesTableLink" href="?c=info&id=<?php echo $room->getId() ?>"><?php echo $room->getNazov(); ?></a>
                        </td>
                        <td><?php echo $room->getKontakt(); ?></td>
                        <td><?php echo $room->getPsc(); ?></td>
                        <td><?php echo $room->getUlica(); ?></td>
                        <td><?php echo $room->getMesto(); ?></td>
                        <td><?php echo $room->getMinutaz(); ?></td>
                        <td style="display: none"><?php echo $room->getOpis() ?></td>
                        <td class="contact-delete">
                            <div class="row">
                                <div class="col">
                                    <form action='?c=info&a=deleteRoom' method="post">
                                        <input type="hidden" name="roomId" value="<?php echo $room->getId(); ?>">
                                        <button class="btn btn-danger" type="submit" name="submit" value="Zmazať">ZMAZAŤ</button>
                                    </form>
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-success editbtn">UPRAVIŤ</button>
                                </div>
                            </div>

                        </td>

                    </tr>
                <?php } ?>


            </table>
        </div>
    </div>

    <div class="infoBiela" id="userFavTable">
        <div id="userFavEscapes">
            Moje obľúbené Escapes
        </div>
        <div class="table-responsive-lg">
            <table class="table">
                <tr>
                    <th>Názov</th>
                    <th>Hodnotenie</th>
                    <th>Kontakt</th>
                    <th>Psč</th>
                    <th>Ulica</th>
                    <th>Mesto</th>
                    <th>Minutáž</th>
                    <th>Akcie</th>
                </tr>
                <?php
                $userRooms = $user->getFavorites();
                foreach ($userRooms as $room) {  ?>
                    <tr>
                        <td>
                            <a class="userEscapesTableLink" href="?c=info&id=<?php echo $room->getId() ?>"><?php echo $room->getNazov(); ?></a>
                        </td>
                        <td><?php echo $room->getHodnotenie() ?></td>
                        <td><?php echo $room->getKontakt(); ?></td>
                        <td><?php echo $room->getPsc(); ?></td>
                        <td><?php echo $room->getUlica(); ?></td>
                        <td><?php echo $room->getMesto(); ?></td>
                        <td><?php echo $room->getMinutaz(); ?></td>
                        <td class="contact-delete">
                            <form action='?c=info&a=deleteFavorite' method="post">
                                <input type="hidden" name="roomId" value="<?php echo $room->getId(); ?>">
                                <button class="btn btn-danger" type="submit" name="submit" value="Zmazať">ZMAZAŤ</button>
                            </form>
                        </td>


                    </tr>
                <?php } ?>


            </table>
        </div>
    </div>

    <!-- MODAL EDIT -->
    <div class="modal fade text-black" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upraviť escape room</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" id="updateEscapeForm" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" name="roomId" id="updateRoom_id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Názov</label>
                                    <input class="form-control" type="text" name="name" placeholder="Názov" id="nameUpdate">
                                    <!--                        <span class="error">--><?php //echo $nazovError;?><!--</span>-->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Kontakt</label>
                                    <input class="form-control" type="tel" name="contact" id="contactUpdate" placeholder="+421.." required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>PSČ</label>
                                    <input class="form-control" type="text" name="psc" id="pscUpdate" placeholder="PSČ" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Ulica</label>
                                    <input class="form-control" type="text" name="street" id="streetUpdate" placeholder="Ulica" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Mesto</label>
                                    <input class="form-control" type="text" name="town" id="townUpdate" placeholder="Mesto" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Minutáž</label>
                                    <input class="form-control" type="text" name="minutes" id="minutesUpdate" placeholder="Minutáž" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Opis</label>
                                    <textarea class="form-control" id="descUpdate" name="desc" placeholder="Pre zachovanie pôvodného opisu nechajte toto pole prázdne!"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label>Obrázok</label>
                                    <input class="form-control" type="file" name="file" id="fileUpdate">
                                </div>
                                <div class="mb-3">
                                    Pre zachovanie pôvodného obrázku nenahrávajte žiadny súbor.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zavrieť</button>
                        <button type="submit" name="updateEscape" id="updateEscape" class="btn btn-warning">Uložiť</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php } ?>
