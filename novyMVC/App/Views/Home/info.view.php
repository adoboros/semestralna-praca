<?php /** @var Array $data */ ?>
<div class="infoBiela">
    <div class="container">
        <div class="d-flex justify-content-between">
            <?php $room = $data['room'];
            $nazov = $room->getNazov();
            $mesto = $room->getMesto();
            $minutaz = $room->getMinutaz(); ?>
            <div class="card">
                <img src="<?= \App\Config\Configuration::UPLOAD_DIR . $room->getImage() ?>" class="img-fluid" alt="Náhľad nie je k dispozícii.">
                <div class="card-body">
                    <h5 class="card-title" id="previewName"><?php echo $nazov ?></h5>
                    <p class="card-text" id="previewTown"><?php echo $mesto ?></p>
                    <p class="card-text" id="previewTime"></p><?php echo $minutaz ?> minút</p>
                </div>

            </div>

            <div class="mainInfo">
                <div class="yellowText">
                Názov: <?php echo $nazov ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        Mesto: <div class="yellowText"><?php echo $mesto ?></div>
                    </div>
                    <div class="col-md-6">
                        Psč: <?php echo $room->getPsc() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Ulica: <?php echo $room->getUlica() ?>
                    </div>
                    <div class="col-md-6">
                        Kontakt: <?php echo $room->getKontakt() ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Trvanie: <?php echo $minutaz ?> minút
                    </div>
                    <div class="col-md-6">
                        Hodnotenie: <?php echo $room->getHodnotenie() ?> / 5
                    </div>
                </div>

            </div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<div class="infoSeda">
    <h1>Opis</h1>
    <div class="description">
        <?php echo $room->getOpis() ?>
    </div>

</div>

<div class="infoBiela">
    <div class="container">
        <h1>Komentáre</h1>

    <?php foreach ($room->getComments() as $comment) { ?>
        <div class="d-flex justify-content-center">
            <div class="comment mt-4 text-justify" id="comment">
                <h4><?php $owner = $comment->getOwner()[0];
                    echo $owner->getUsername(); ?></h4> <span>- <?php echo $comment->getDate() ?></span> <br>
                <p><?php echo $comment->getText() ?></p>
            </div>
        </div>
    <?php } ?>
        <div class="d-flex justify-content-center">
            <form id="newCommentTextArea">
                <textarea class="form-control" placeholder="Váš komentár" required ></textarea>
                <button type="submit" class="btn btn-warning" value="Odoslať">Odoslať</button>
            </form>
        </div>
    </div>
</div>