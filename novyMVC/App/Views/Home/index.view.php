<?php /** @var Array $data */ ?>
<script src="public/addEscapeAjax.js"></script>
<div class="infoBiela">
    <h1>
        Na čo slúži stránka Escape rooms?
    </h1>
    <div>
        Stránka je tvorená pre milovníkov únikových miestností na Slovensku.
    </div>
    <div class="lovers">
        Nachádza sa tu databáza väčšiny slovenských "escapes".
    </div>
    <div>

    </div>
</div>

<div class="infoSeda">

    <h2>
        Unikneš zo všetkých?
    </h2>

    <div class="container-sm">
        <img class="img-fluid" src="public/images/lock.png" alt="Obrázok zámky">
    </div>



</div>

<div class="preview">
    <div class="container" style="margin-top: 5rem;">
        <div class="escapesList" id="countUpStart">Escape Rooms</div>
        <form id="formSearch" method="POST" action="?c=home&a=search">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3">
                        <input type="text" name="escapeSearchTown" id="searchTown" placeholder="Hľadané mesto">
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-warning" value="Odoslať">Vyhľadaj</button>
                    </div>
                </div>


            </div>

        </form>

        <div class="row">
            <div class="d-flex justify-content-start flex-wrap">
                <?php
                foreach ($data['rooms'] as $room) {  ?>
                    <div class="card">
                        <img src="<?= \App\Config\Configuration::UPLOAD_DIR . $room->getImage() ?>" class="card-img-top" alt="Náhľad nie je k dispozícii.">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $room->getNazov() ?></h5>
                            <p class="card-text"><?php echo $room->getMesto() ?></p>
                            <p class="counter" data-target="<?php echo $room->getMinutaz() ?>">0</p>
                            <?php if (\App\Auth::isLogged()) { ?>
                            <a href="?c=info&id=<?php echo $room->getId() ?>" class="btn btn-warning stretched-link">Detail</a>
                            <?php  } ?>
                        </div>
                    </div>
                <?php } if (\App\Auth::isLogged()) { ?>
                <div class="card" id="noCardBorder">
                    <div class="card-body align-items-center d-flex justify-content-center">
                        <!-- Button trigger modal -->

                        <button class="addingButton" type="button" data-bs-toggle="modal" data-bs-target="#escapeAddModal" data-backdrop="static" data-keyboard="false" id="addEscapeButtonModal">
                            <i class="fas fa-plus fa-5x"></i>
                        </button>
                    </div>

                </div>
                <?php  } ?>

            </div>
        </div>
    </div>
</div>
<!-- Modal pridať-->

<div class="modal fade text-black" id="escapeAddModal" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pridať novú escape room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form id="form_id" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Názov</label>
                                <input class="form-control" type="text" name="name" placeholder="Názov" id="nameAdd">
                                <!--                        <span class="error">--><?php //echo $nazovError;?><!--</span>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Hodnotenie</label>
                                <input class="form-control" type="number" step="0.1" max="5" min="0" name="rating" placeholder="0-5" id="ratingAdd">
                                <!--                        <span class="error">--><?php //echo $hodnotenieError;?><!--</span>-->
                            </div>
                        </div>
                    </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label>Kontakt</label>
                            <input class="form-control" type="tel" name="contact" placeholder="+421.." required id="contactAdd">
                            <!--                        <span class="error">--><?php //echo $kontaktError;?><!--</span>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label>PSČ</label>
                            <input class="form-control" type="text" name="psc" placeholder="PSČ" required id="pscAdd">
                            <!--                        <span class="error">--><?php //echo $pscError;?><!--</span>-->
                        </div>
                    </div>
                </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Ulica</label>
                                <input class="form-control" type="text" name="street" placeholder="Ulica" required id="streetAdd">
                                <!--                        <span class="error">--><?php //echo $ulicaError;?><!--</span>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Mesto</label>
                                <input class="form-control" type="text" name="town" placeholder="Mesto" required id="townAdd">
                                <!--                        <span class="error">--><?php //echo $mestoError;?><!--</span>-->
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Minutáž</label>
                                <input class="form-control" type="number" name="minutes" placeholder="Minutáž" id="minutesAdd" required>
                                <!--                        <span class="error">--><?php //echo $minutazError;?><!--</span>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label>Obrázok</label>
                                <input class="form-control" type="file" name="file" id="file" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3">
                            <label>Opis</label>
                            <textarea class="form-control" id="descAdd" name="desc" placeholder="Opis escape room" required></textarea>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zavrieť</button>
                    <button class="btn btn-warning" id="addEscape">Uložiť</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL EDIT -->
<!--<div class="modal fade text-black" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upraviť escape room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="?c=Home&a=update" method="post">
                <div class="modal-body">
                    <input type="hidden" name="update_id" id="id">
                    <div class="mb-3">
                        <label>Názov</label>
                        <input class="form-control" type="text" name="nazov" id="nazov" placeholder="Názov" required>

                    </div>
                    <div class="mb-3">
                        <label class="form-label">Hodnotenie</label>
                        <input class="form-control" type="number" step="0.1" max="5" min="0" name="hodnotenie" id="hodnotenie" placeholder="Hodnotenie">
                    </div>
                    <div class="mb-3">
                        <label>Kontakt</label>
                        <input class="form-control" type="tel" name="kontakt" id="kontakt" placeholder="+421.." required>
                    </div>
                    <div class="mb-3">
                        <label>PSČ</label>
                        <input class="form-control" type="text" name="psc" id="psc" placeholder="PSČ" required>
                    </div>
                    <div class="mb-3">
                        <label>Ulica</label>
                        <input class="form-control" type="text" name="ulica" id="ulica" placeholder="Ulica" required>
                    </div>
                    <div class="mb-3">
                        <label>Mesto</label>
                        <input class="form-control" type="text" name="mesto" id="mesto" placeholder="Mesto" required>
                    </div>
                    <div class="mb-3">
                        <label>Minutáž</label>
                        <input class="form-control" type="text" name="minutaz" id="minuty" required>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zavrieť</button>
                    <button type="submit" name="upravitEscape" class="btn btn-warning">Uložiť</button>
                </div>
            </form>
        </div>
    </div>
</div>-->



