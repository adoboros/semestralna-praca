<link rel="stylesheet" href="public/loginCss.css">
<script src="public/loginAjax.js"></script>

<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3>Prihlásiť</h3>
                <div class="d-flex justify-content-end social_icon">
                    <span><i class="fab fa-facebook-square"></i></span>
                    <span><i class="fab fa-google-plus-square"></i></span>
                    <span><i class="fab fa-twitter-square"></i></span>
                </div>
            </div>
            <div class="card-body">
                <form id="login_form">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="e-mail" id="login">

                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="heslo" id="password">
                    </div>
                    <div class="row align-items-center remember">
<!--                        <input type="checkbox">Pamätať si ma-->
                    </div>
                    <div class="form-group">
                        <input type="submit" value="PRIHLÁSIŤ" class="btn float-right login_btn" id="login_btn">
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    Ešte nemáte účet?<a href="?c=auth&a=registerForm">Registrovať sa</a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="?c=auth&a=registerForm">Zabudli ste heslo?</a>
                </div>
            </div>
        </div>
    </div>
</div>
