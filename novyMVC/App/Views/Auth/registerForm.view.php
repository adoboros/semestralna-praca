<link rel="stylesheet" href="public/loginCss.css">
<script src="public/registerAjax.js"></script>

<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header">
                <h3>Registrovať</h3>
                <div class="d-flex justify-content-end social_icon">
                    <span><i class="fab fa-facebook-square"></i></span>
                    <span><i class="fab fa-google-plus-square"></i></span>
                    <span><i class="fab fa-twitter-square"></i></span>
                </div>
            </div>
            <div class="card-body">
                <form id="registration_form">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="meno" id="username">

                    </div>
                    <!--<div class="input-group mb-2 errorDiv">
                        <div class="input-group-text" id="usernameError">0</div>
                    </div>-->
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="email" class="form-control" placeholder="e-mail" id="email">
                        <div id="emailError"></div>
                    </div>
                    <!--<div class="input-group mb-2 errorDiv">
                        <div class="input-group-text" id="usernameError">0</div>
                    </div>-->
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="heslo" id="password">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="zopakovať heslo" id="secondPassword">
                    </div>
<!--                    <div class="row align-items-center remember">
                        <input type="checkbox">Pamätať si ma
                    </div>-->
                    <div class="form-group">
                        <input type="submit" value="REGISTROVAŤ" class="btn float-right register_btn" id="register_btn">
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div id="success" class="col-sm-offset-3 col-sm-6 m-t-15"></div>
                <!--<div class="d-flex justify-content-center links">
                    Ešte nemáte účet?<a href="#">Registrovať sa</a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="#">Zabudli ste heslo?</a>
                </div>-->
            </div>
        </div>
    </div>
</div>
