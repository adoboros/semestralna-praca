<?php ?>

<link rel="stylesheet" href="public/loginCss.css">
<script src="public/changePasswordAjax.js"></script>

<div class="container">
    <div class="d-flex justify-content-center">
        <div class="card">
            <div class="card-header">
                <h3>Zmena hesla</h3>
            </div>
            <div class="card-body">
                <form id="changePasswordForm" method="post">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Aktuálne heslo" id="oldPassword">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Nové heslo" id="password">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control" placeholder="Zopakovať nové heslo" id="secondPassword">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ZMENA HESLA" class="btn float-right changePassword_btn" id="changePassword_btn">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
