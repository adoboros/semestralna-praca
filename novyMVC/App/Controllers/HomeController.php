<?php

namespace App\Controllers;

use App\Core\AControllerBase;
use App\escapeHandler;
use App\inputSQLIAvoidHandler;
use App\Models\EscapeRoom;
use App\searchHandler;

/**
 * Class HomeController
 * Example of simple controller
 * @package App\Controllers
 */
class HomeController extends AControllerRedirect
{

    public function index()
    {
        if (searchHandler::isSearching())
        {
            //$searchName = $this->request()->getValue('escapeSearchName');
            $searchTown = $_SESSION['searchTown'];
            $rooms = EscapeRoom::getAll('mesto LIKE ?', [$searchTown.'%']);
        } else
        {
            $rooms = EscapeRoom::getAll();
        }
        searchHandler::unsetSearchState();
        return $this->html(
            [
                'rooms' => $rooms
            ]);
    }

    public function search()
    {
        $searchTown = $this->request()->getValue('escapeSearchTown');
        if (!inputSQLIAvoidHandler::checkInput($searchTown)) {

            searchHandler::setSearchState($searchTown);
        }
        $this->redirect('home');
    }

    public function contact()
    {
        return $this->html(
            []
        );
    }

    /*public function info()
    {
        $roomId = $this->request()->getValue('id');
        $room = EscapeRoom::getOne($roomId);
        return $this->html(['room' => $room]);
    }*/



    public function update()
    {
        $roomId = $this->request()->getValue('id');

        $room = EscapeRoom::getOne($roomId);
        $this->setRoom($room);
        $room->save();
        $this->redirect('home');
    }

    public function updateRoom()
    {
        $response = Array();
        $roomId = $this->request()->getValue('roomId');
        $name = $this->request()->getValue('name');
        $town = $this->request()->getValue('town');
        $street = $this->request()->getValue('street');
        $minutes = $this->request()->getValue('minutes');
        $psc = $this->request()->getValue('psc');
        $contact = $this->request()->getValue('contact');
        $desc = $this->request()->getValue('desc');
        $response = escapeHandler::updateRoom($roomId, $name, $contact, $psc, $street, $town, $minutes, $desc);
        return $this->json($response);
    }

    public function insertRoom()
    {
        $response = Array();
        if (isset($_FILES['file']))
        {
            $name = $this->request()->getValue('name');
            $town = $this->request()->getValue('town');
            $street = $this->request()->getValue('street');
            $minutes = $this->request()->getValue('minutes');
            $psc = $this->request()->getValue('psc');
            $contact = $this->request()->getValue('contact');
            $rating = $this->request()->getValue('rating');
            $desc = $this->request()->getValue('desc');
            $response = escapeHandler::addRoom($name, $rating, $contact, $psc, $street, $town, $minutes, $desc);
        } else
        {
            $response['fileError'] = 'Chyba nahrávania súboru.';
        }

        return $this->json($response);

        /*$room = new EscapeRoom();
        $this->setRoom($room);
        $room->save();
        $this->redirect('home');*/
    }

    protected function setRoom(EscapeRoom $room)
    {
        $nazov = $this->request()->getValue('nazov');
        $hodnotenie = $this->request()->getValue('hodnotenie');
        $kontakt = $this->request()->getValue('kontakt');
        $psc = $this->request()->getValue('psc');
        $ulica = $this->request()->getValue('ulica');
        $mesto = $this->request()->getValue('mesto');
        $minutaz = $this->request()->getValue('minutaz');

        $room->setNazov($nazov);
        $room->setHodnotenie($hodnotenie);
        $room->setKontakt($kontakt);
        $room->setPsc($psc);
        $room->setUlica($ulica);
        $room->setMesto($mesto);
        $room->setMinutaz($minutaz);
    }
}