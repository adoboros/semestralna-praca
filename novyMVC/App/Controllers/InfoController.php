<?php

namespace App\Controllers;

use App\Auth;
use App\Core\Responses\Response;
use App\escapeHandler;
use App\favoritesHandler;
use App\Models\EscapeRoom;
use App\Models\User;

class InfoController extends AControllerRedirect
{

    /**
     * @inheritDoc
     */
    public function index()
    {
        $roomId = $this->request()->getValue('id');
        $room = EscapeRoom::getOne($roomId);
        return $this->html(['room' => $room]);
    }

    public function profile()
    {
        $user_email = Auth::getLoggedEmail();
        $user = User::getAll('email = ?', [$user_email]);
        return $this->html(
            ['user' => $user,
                'error' => $this->request()->getValue('error')]
        );
    }


    public function deleteRoom()
    {
        $roomId = $this->request()->getValue('roomId');

        if (is_numeric($roomId))
        {
            escapeHandler::deleteRoom($roomId);
        }

        $this->redirect('Info', 'profile');
    }

    public function addFavorite()
    {
        $room_id = $this->request()->getValue('id');
        favoritesHandler::addFavorite($room_id);
        $this->redirect('info', 'index', ['id' => $room_id]);
    }

    public function deleteFavorite()
    {
        $room_id = $this->request()->getValue('roomId');
        favoritesHandler::deleteFavorite($room_id);
        $this->redirect('info', 'profile');
    }

    public function addRating()
    {
        $rating = $this->request()->getValue('rating');
        $roomId = $this->request()->getValue('id');
        escapeHandler::addRating($roomId, $rating);
        $this->redirect('info','index',['id' => $roomId]);

    }

    public function addComment()
    {
        $roomId = $this->request()->getValue('id');
        $commentText = $this->request()->getValue('commentText');
        escapeHandler::addComment($roomId, $commentText);
        $this->redirect('info','index',['id' => $roomId]);
    }

}