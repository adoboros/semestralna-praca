<?php

namespace App\Controllers;

use App\Auth;
use App\Core\Responses\Response;

class AuthController extends AControllerRedirect
{

    /**
     * @inheritDoc
     */
    public function index()
    {
        // TODO: Implement index() method.
    }

    public function loginForm()
    {
        return $this->html();
    }

    public function login()
    {
        $login = $this->request()->getValue('login');
        $password = $this->request()->getValue('password');
        return $this->json(Auth::login($login, $password));
    }

    public function changePasswordForm()
    {
        return $this->html([]);
    }

    public function changePassword()
    {
        $oldPassword = $this->request()->getValue('oldPassword');
        $password = $this->request()->getValue('password');
        $secondPassword = $this->request()->getValue('secondPassword');
        return $this->json(Auth::changePassword($oldPassword, $password, $secondPassword));
    }

    public function logout()
    {
        Auth::logout();
        $this->redirect('home');
    }

    public function registerForm()
    {
        return $this->html([]);
    }


    public function register()
    {
        $username = $this->request()->getValue('username');
        $email = $this->request()->getValue('email');
        $password = $this->request()->getValue('password');
        $response =  Auth::register($username, $email, $password);
        if (count($response) == 0)
        {
            $response["successfull"] = "Registrácia prebehla úspešne.";
        }
        return $this->json($response);

    }

    public function deleteUser()
    {
        //unset session, delete all user rooms, delete user
        $passwordConfirm = $this->request()->getValue('deletePassConfirm');
        if (Auth::deleteUser($passwordConfirm))
        {
            $this->redirect('home');
        } else {
            $this->redirect('info', 'profile', ['error' => 'Nesprávne heslo']);
        }

    }


}